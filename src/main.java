//Chris Hinson
//Chapter 4 Workbench 19

import java.io.*;
import java.util.Scanner;

public class main {
    public static void main(String args[]) throws IOException
    {
        int total = 0;
        File file = new File("C:\\Users\\Hinson.christopher\\IdeaProjects\\ch4wkbh17\\NumberList.txt");
        Scanner fileScanner = new Scanner(file);

        while (fileScanner.hasNext())
        {
            total += fileScanner.nextInt();
        }

        System.out.println(total);
    }
}
